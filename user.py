#!/data/data/com.termux/files/usr/bin/python 
# coding: utf-8

# project shouty
#author: brouss@tutanota.com
#start date: 2019/01/02
#end date :  ???
#version:   0.0

from time import time


# generic funcs

#from string to float or bool
def floatOrBool(x):
    try:
        return(float(x))
    except:
        return(bool(x))


#error codes:
errCodes=[0]*1000
errCodes[ 0] = "OK"
errCodes[ 1] = "You are not validated/did not accept"
errCodes[ 2] = "You are banned"
errCodes[ 3] = "You cant write"
errCodes[ 4] = "You cant administrate"
errCodes[ 5] = "You cant modify root" 
errCodes[ 6] = "You cant modify refusing user"
errCodes[ 7] = "You need to be root"
errCodes[ 8] = "Only root can ban an admin"
errCodes[ 9] = "Was already done"
errCodes[10] = "Phone already registred"
errCodes[11] = "You cant administrate yourself"

#usr status can be
# I(nvited)
# V(alid)
# S(ubmitted)
# D(enied, by adm)
# R(efused, by self)
users_file      = "users.txt"

root_user="+33612345678\tOperator\t0\tV\tTrue\tTrue\tFalse"
#           phone nb    name    id stt writ  adm   banned

#TODO make a real last id and never reuse        
class User:
    hUsers={}
    hPhones={}
    hNames={}
    usersLastId=0

    #user creation from line formatted as users.txt
    def __init__(self,data):
        data=data.split("\t")
        self.phone=data.pop()
        self.name=data.pop()
        id=int(data.pop())
        if id<0:
            id=User.usersLastId+1
        self.id=id
        User.usrsLastId=max(id,User.usrsLastId)
        self.status=data.pop()
        self.writer=bool(data.pop())
        self.admin = bool(data.pop())
        self.banned = floatOrBool(data.pop())
        User.hUsers[id]=self
        User.hPhones[self.phone]=self
        if self.name!="":
            User.hNames[self.name]=self

    #return a users.txt compliant str
    def toString(self): #TODO: make it match with init
        return("\t".join([self.phone,
                        self.name,
                        str(self.id),
                        self.status,
                        str(self.writer)
                        str(self.admin),
                        str(self.banned)  ]))

    #returns a boolean
    def isBanned(self):
        if type(self.banned)==bool:
            return self.banned
        if time() > self.banned:
            self.banned=False
            return(False)
        return(True)

    #returns an err code. 0=OK
    def canRead(self):
        if self.status!="V": #you invalid
            return(1) 
        if self.isBanned():
            return(2) #you banned
        return(0)

    #returns an err code. 0=OK
    root and writers can write
    def canWrite(self):
        if self.id!=0:
            cr=self.canRead()
            if cr:
                return(cr)
            if not self.writer:
                return(3) #"you cant write"
        return(0)

    #returns an err code. 0=OK if active adm with xtra cnd % user
    #root and admins can do adm. usr=None if just asking if has right to
    def canAdm(self,user=None):
        if self.id!=0:
            cr=self.canRead()
            if cr:
                return(cr)
            if not self.admin:
                return(4) #"you cant administrate"
        if user!=None:
            if user.id==0 and self.id!=0:      
                return(5)  #cant modify root
            if user.id==self.id and self.id!=0:      
                return(11)  #cant admin self
            if user.status=="R": 
                return(6) #cant modify refusing user
        return(0)
    
    #returns an err code. 0=OK
    #TODO: chk nb cleaned?
    def invite(self,nb):
        ca=self.canAdm()
        if ca:
            return(ca)
        if nb.startswith("0"):
            nb=User.hUsers[0].phone[:3]+nb[1:]
        if nb in User.hHphones:
            return(10)
        #nb    name    id stt writ  adm   banned
        newU=User("%s\t\t-1\tI\tFalse\tFalse\tFalse"%nb)
        return(0)

    #returns an err code. 0=OK
    #root and valid adm can do it
    def setWrite(self,user=None,state=None):
        assert((user==None) == (state==None))
        if self.id!=0!
            ca=self.canAdm(user)
            if ca:
                return(ca)
        if user!=None:
            if user.writer==state:
                return(9)   #nothing to do
            user.writer=state
#            writeUsers()
        return(0)
        
    #returns an err code. 0=OK
    #can be one by root or valid admin
    # usr=None -> just asking. duration = None -> permaban
    def setBan(self,user=None,duration=None):
        ca=self.canAdm(user)
        if ca!=0:
            return(ca)
        if user!=None:
            if user.admin and self.id!=0:
                return(8) #only root can ban admin
            if duration==None:
                banned=True
            elif duration==0:
                banned=False
            else:
                banned=time()+duration
            if banned==user.banned:
                return(9)   #nothing to do
            user.banned=banned
            writeUsers()
        return(0)
    
    #returns an err code. 0=OK
    #can be done by root only
    def setAdm(self,user,state):
        if self.id!=0:
            return(7)
        if user.status=="R":
            return(6)
        if user.admin==state:
            return(9)
        user.admin=state
        writeUsers()
        return(True)
    
    #returns a list of allowed command codes
    def getCmds(self=None):
        res=["help"]
        if self==None:  #case user has no account
            res.append("subscribe")
            return(res)
        if self.status=="D": #denied. cant do shit
            return([])  
        res.append("status")
        if self.status=="S"  or self.isBanned() #banned must wait unban
            return(res)         #self submitted usr must wait validation
        if self.status=="I"  #invited
            res.append("accept")
            res.append("refuse")
            return(res)
        if self.status=="R"  #Refuse
            res.append("accept")
            return(res)
        if self.status=="V":
            if self.id!=0:
                res.append("unsubscribe")
            if self.id==0:
                res.append("addAdm")
                res.append("delAdm")
            if self.admin or self.id==0 or self.writer:
                res.insert(2,"status2")
            if self.admin or self.id==0:
                res.append("invite")
                res.append("ban")
                res.append("addWrt")
                res.append("delWrt")
            if self.id==0 or self.writer:
                res.append("say")
            return(res)
        return([])


            
#user file structure in order to read old files as file format evolves
users_fileVer   = 0 

def readUsers(users_file):
    try:
        f=open(users_file)
    except:
        return([])  #should go on
    User(root_operator) #root does not rely on the file but in hardcoded string above.
    #headers
    try:
        fileVer=int(f.readline().strip())
    except:
        return(0)   #should halt. Unexpected file
    try:
        User.usersLastId=int(f.readline().strip())
    except:
        return(0)
    #data
    l=f.readline()  
    while l:
        User(l.strip())
        l=f.readline()
    f.close()

def writeUsers(users_file):
    try:
        f=open(users_file,"w")
        #file headears
        f.write("%d\n%d\n"%(users_fileVer,User.usersLastId))
        #file data
        l=list(User.hUsers.keys())
        l.sort()
        l.pop(0)
        for uid in l:
            f.write(User.hUsers[uid].toString()+"\n")
        f.close()
        return(True)
    except:
        return(False)

def getUserByPhone(phone):
    try:
        return(User.hPhones[phone])
    except:
        return(None)

def getUserByName(name):
    try:
        return(User.hNames[name])
    except:
        return(None)

