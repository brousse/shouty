#!/data/data/com.termux/files/usr/bin/python 
# coding: utf-8

# project shouty
#author: brouss@tutanota.com
#start date: 2019/01/02
#end date :  ???
#version:   0.0

from os import system
from subprocess import getoutput
from time import time,sleep,strptime,mktime,strftime,gmtime

from smsRead import smsRead
import user

log_file        = "/data/data/com.termux/files/usr/var/log/shouty.log"
sleep_len       = 60.0

#english language keywords:
KWcaller =   "shouty"
KWsub =     "subscribe"
KWunsub =   "unsubscribe"
KWinvit =   "invite"
KWshout =   "shout"
KWlist =    "list"

#overloadable values:



#timefiltered already.
def filterSmss(lsms):
    lsms2=[]
    for sms in lsms:
        if sms.phone=="" or sms.tim==0:
            continue
        msg = sms.msg.split()
        if len(msg)<2:
            continue
        if msg[0].lower() != KWcaller:
            continue
        sid=getUser(sms.phone)  #sender id
        if sid==None:
            sms.sender=None
        else:
            sms.sender=User.hUsers[sid]
        #msg must be dealt with
        sms.command=msg[1].lower()
        sms.msg=msg[2:]
        lsms2.append(sms)
    return(lsms2)

def subscribe(sms):
    msg=sms.msg.split()
    if len(msg)!=3:
        #TODO: reply "must specify username
        continue
    else:
        res=
    if sid==None:
        #case unknown user:
        #TODO: create user as waiting validation.
        #TODO: send validation request to adms
    elif not sender.isInvited:
        #TODO: reply are already subscribed[+ status]
    elif getUserByName()
    else:
        sender.isInvited=False
        #TODO make user active
        #TODO: send validated msg to sender+help
        #TODO! send new status to admins
    
#TODO: read all in one chunk to avoid errors caused by new sms between chunks
def readSmss(oldest):
    global nbSmsRead
    omit=0
    cont=True
    ret=[]
    while cont:
        lsms=smsRead(nbSmsRead,omit)
        if lsms==None:
            #TODO: deal with couldnt access sms
            return
        if len(lsms)==0:
            return(ret)
        i=0
        while len(lsms)>i and lsms[i].tim <= oldest:
            cont=False
            i+=1
        omit+=len(lsms)
        ret=filterSmss(lsms[i:])+ret
    return(ret)        
        
        
user.readUsers()
lastRun=time()
while True:
    sleep(sleep_len)
    lsms=readSmss(lastRun)
    atLeastOneMod=False
    for sms in lsms:
        if sms.command==KWsub:
            subscribe(sms)
        elif sms.command==KWunsub:
            unsubscribe(sms)
    lastRun=time()