# Shouty

This is a program to turn an android phone line into an SMS platform. Basic administration too is done through SMS. 
Heavier stuff should be done by hand editing the DB files. (or someone explain me how to install mariadb on termux).

It is in its early stages and does not do anything at the current date.

Here is how it should work:
On command, a user sending a message to the service would have his message sent back to everyone.

There are 5 "levels" of users:
- operator is a super admin. The owner of the phone line is the operator.
- admin is self explanatory
- writer is people who can read and write through the service
- reader is people who can only read but not send messages. 
- 'invited' and 'validating' is people having received an invitation and did not accept nor decline. Can only subscribe or decline


# Rules:

This chapter is subject to changes depending on the scale of the test. 

In a first test run, it will be limited to:
- 100 users
- 20 channels  
- 10 admins
- only owner creates channels and admins
- owner de facto is an admin
- only one private channel: admins
- reader status: dual requester's validation or admin's invitation accepted
- writer status automatic after 1 day lurker
- owner can ban/mute anyone permanently or for a duration
- admin can ban/mute any non admin for duration up to 48 hours
- anyone can report a message to owner

# commands:

based on keywords that can be changed depending on the users language.
Messages should always start with the daemon name as a sign that this sms is to be dealt.
Commands are sent to the servers via SMS. 
examples:

- \<service\> subscribe \<username\>

    subscribe oneself as \<username\>. An sms is sent to the person stating that an adm validation is still needed except if the subscribers phone # is in the contact list of operator in which case, user is accepted by default.
- \<service\> unsubscribe

    unsubscribe oneself. The account is not really deleted (for historic/legal reasons) but is removed from sending/receiving lists.
- \<service\> [un]mute \< username | phone# \> [duration in hours]

    reserved to admins
    switches a user from reader to writer or back; if a duration is given, the other state will be put after the <duration>
- \<service\> invite \<phone nb\>

    reserved to admins
    puts an unknown user in the invited list. The person is contacted and proposed to subscribe or unsubscribe
- \<service\> list users

    depending of the requesting status, different data are given:
    reader -> list admins (+ operator) & list active users (readers and writers mixed)
    writer -> list admins (+ operator) & list active readers and active writers
    admin/operator -> list operator; list adms; list writers (with duration if applicable and star if disabled) list readers (same)
- \<service\> list topics

    topics are rooms from which users can receive messages.
    topic "all" concerns all active subscribers. Other topics are subject to subscription
    Some topics are "hidden" for people not to subscribe
    only admins/operators get the hidden topics when listing
- \<service\> list commands or \<service\> help
 
    lists commands available to the user
- \<service\> [un]join \<topic\>

    allows reader to [not] receive messages sent to the topic
- \<service\> add \<topic\>

    reserved to admins.
    create a new topic. Only the creator can create hidden topics.
- \<service\> say \<topic\>: \<message\>

    only available to writers, adms and operator
    the heart of it all. the message is resent to all subscribers who joined \<topic\>
- \<service\> shout \<msg\>

    only available to writers, adms and operator
    sends to all active users

