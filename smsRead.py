#!/data/data/com.termux/files/usr/bin/python 
# coding: utf-8

# project shouty
#author: brouss@tutanota.com
#start date: 2019/01/02
#end date :  ???
#version:   0.0

from subprocess import getoutput
from time import mktime,strptime

sms_time_fmt    = "%Y-%m-%d %H:%M"  #sms reception time as shown in termux-api


class Sms:
    def __init__(self, phone,name,tim,msg):
        self.phone=phone
        self.name=name
        self.tim=tim
        self.msg=msg #" ".join(msg.split())

#returns a list of sms objects
def smsRead(nbSmsRead,omit):
    try:
        txt=getoutput("termux-sms-list -l %d -o %d"%(nbSmsRead,omit))
    except:
        return(None) #either access error (to sms or contacts) or termux-api missing
    lsms=[]
    name=msg=when=nb=""
    for line in txt.split("\n"):
        if ":" in line:
            fil,val=line.split(":",1)
            fil=fil.strip()
            if "sender" in fil:      #info comes from contact list
                name=val[2:-2]
            if "number" in fil:     #calling number
                nb=val[2:-2]
            if "body" in fil:      #SMS content. Seek keyword.  note "-1" below
                msg=val[2:-1]) # -1 bc no ",": this is the last field
            if "received" in fil:   #Received_time
                try:
                    when=mktime(strptime(val[2:-2],fmt))
                except:
                    #invalid received time:
                    when=0
        elif "}" in line:           #denotes end of an SMS. 
            lsms.append(Sms(nb,name,when,msg))
            name=msg=when=nb=""
    return(lsms)
